---
date: 2021-02-17
author: Akanoa
tags:
    - programmation
    - gitops
sidebarDepth: 4
permalink: /kubernetes-gitlab
---

# Installer un executor kubernetes Gitlab sur un microk8s 

Bonjour à tous :D 

Aujourd'hui on va faire de l'automatisation et un peu de gitops; un chouille.

Je vous propose d'installer un runner gitlab en autoscale sur un cluster kubernetes via la distribution microk8s.
 
> Il y a 12 mille mots incompréhensible là, c'est quoi Gitlab ?

Gitlab est ce que je considère comme une plateforme de gitops...

> Mais c'est quoi gitops?

Le gitops est un paradigme de travail qui s'appuie sur le merveilleux outils qu'est git (je vous ferai pas l'affront de vous expliquer git). Il permet
au moyen d'observation d'évènements relatifs à git (push, merge, ...) d'automatiser des tâches, ce qui est à la base de l'intégration continue (CI) et du
déploiement continue(CD).

L'idée est de rendre les choses les plus simples possibles aux développeurs. Ils poussent leur code et des robots bossent pour s'assurer de la qualité du boulot et
d'envoyer en production si les critères de qualité sont respectés.

Sauf que pour ça il faut des robots, et bien les gitlab runners sont ces robots. Ils vont prendre les tâches et les réaliser. Il existe 
tout un tas de [façons](https://docs.gitlab.com/runner/install/) d'installer un runner (il peut d'ailleurs être local à la machine du dev).

C'est bien mais ça a un défaut: ça ne s'adapte pas à la charge de travail, si vous avez 3 tâches à réaliser, il vous faudra 3 runners 
qui tournent quelque part. C'est pas pratique :/

C'est ici qu'entre en scène l'exécutor kubernetes gitlab, il va prendre en charge la création et la destruction des runners après utilisation, ce qui ....

> Oh! Doucement Simone! C'est quoi kubernetes ? Et t'as parlé de microk8s dans le titre c'est quoi encore ça ?

Kubernetes est un produit initialement lancé par google qui a fini par faire consensus dans la communauté des développeurs
pour devenir la manière standard d'orchestrer des containers.

Vu que tu vas me poser la question, un container est une manière d'isoler un processus au sein d'un OS. Non ça n'a rien à voir
avec une VM. C'est plus une cage logiciel qu'autre chose. (pardon pour les puristes, je ferai un article plus détaillé un jour).

Kubernetes va avoir pour rôle de gérer le cycle de vie des ses containers et toutes ses I/O (système de fichiers, réseaux).

On va donc faire tourner nos runners gitlab sous la forme de containers orchestrés par Kubernetes.

Mais Kubernetes c'est un peu comme linux, il y a plein de distributions faites par des éditeurs différents. Pour le tuto on va
utiliser une distribution appellée [microk8s](https://microk8s.io/) gérée par Canonical, la société derrière Ubuntu.

C'est bon le titre est expliqué ?

> Ouais ça va, mais je te tiens à l'oeil 👀

Fantastique, on peut attaquer ! 🔥

## Installer microk8s

Avant de faire quoi que se soit il nous faut une machine, j'ai choisi une Ubuntu 20.04 dans une droplet [Digital Ocean](https://cloud.digitalocean.com/).

Une fois connecté en ssh avec droits root.

```sh
snap install microk8s --classic
```

Puis 

```sh
microk8s status --wait-ready
```

Si tout se passe bien vous devriez avoir en première ligne:

```
microk8s is running
```

Si c'est bon, vous avez votre kubernetes, sinon vous reporter à la FAQ de microk8s.

On va démarrer plusieurs services sur notre cluster:

```
microk8s enable dashboard dns rbac
```

Petite explication:
- dns: un dns interne basé sur CoreDNS
- dashboard: une web ui de gestion du cluster
- rbac: le système de gestion de droits sur le cluster

Vous pouvez vérifier que tout marche avec

```sh
microk8s kubectl get all
```

Ce qui devrait renvoyer quelque chose comme:

```
NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.152.183.1   <none>        443/TCP   6m47s
```

Maintenant on va configurer le dashboard.

Lors de l'installation des services une indication est apparue à l'écran disant de se rendre à l'[adresse](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md).

Et bien faisons ce qui est dit.

D'abord l'account service:

```yaml
# L'utilisateur root
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
---
# Puis le cluster role binding qui va nous accorder les droits root sur le cluster.
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
```

Vous enregistrez ceci dans fichier `admin-dashbord.yml` par exemple.

Puis vous appliquez les modifications au cluster.

```sh
microk8s kubectl apply -f admin-dashboard.yml
```

Et ça vous répond:

```
serviceaccount/admin-user created
clusterrolebinding.rbac.authorization.k8s.io/admin-user created
```

Il nous faut ensuite le token d'authentification associé à l'admin-user:

```sh
microk8s kubectl -n kube-system get secret $(microk8s kubectl -n kube-system get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
```

Cela va vous cracher une chaine de caractères:

```
eyJH.........
```

Stockez quelques part en sécurité et pas en publique. Ce token est la clef pour faire n'importe quoi sur le cluster !!!

On démarre le dashboard:

```sh
microk8s dashboard-proxy
```

Rendez vous à l'IP de votre machine port 10443. `https://localhost:10443` par exemple.

![](../assets/images/kubernetes-gitlab/kubernetes1.png)

Choisissez bien jeton, rentrez le token de l'admin-user et connexion.

Si vous voyez ça:

![](../assets/images/kubernetes-gitlab/kubernetes2.png)

Félicitations vous avez un cluster tout beau tout propre ! :D

## Installer l'executor Gitlab sur le cluster

Si vous n'avez pas de compte sur [Gitlab](https://gitlab.com/), vous pouvez en créer un
c'est gratos.

Pour l'exemple je vais créer un projet et dedans définir un fichier `.gitlab-ci.yml`.

Ce fichier permet de déclarer les opérations de gitops dont on parlait tout à l'heure.

On va faire des choses stupides mais c'est pour la démo.

```yaml
image: busybox

stages:
- step1
- step2

job1:
    stage: step1
    script:
        - sleep 10

job2:
    stage: step2
    script:
        - sleep 10

job2:
    stage: step2
    script:
        - sleep 10

job3:
    stage: step2
    script:
        - sleep 10

job4:
    stage: step2
    script:
        - sleep 10
```

Tout d'abord on va désactiver les runners partagés et ajouter notre cluster Kubernetes

![](../assets/images/kubernetes-gitlab/gitlab1.png)
![](../assets/images/kubernetes-gitlab/gitlab2.png)

Ignorez la pub, notre cluster est mieux 😝

![](../assets/images/kubernetes-gitlab/gitlab3.png)
![](../assets/images/kubernetes-gitlab/gitlab4.png)

On récupère d'abord le port de connexion de l'API de Kubernetes.

```sh
microk8s kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/
{print $NF}'
```

Ici j'ai le 16443

Dans le champ de "API URL", vous allez donc par exemple mettre:

```
https://localhost:16443
```

Ensuite il nous faut un certificat que l'on va extraire des secrets du cluster.

```sh
microk8s kubectl get secrets
```

l'un d'eux devrait se nommer:

```
default-token-xxxx
```

Faites ensuite:

```sh
microk8s kubectl get secret <default-token-xxxx> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
```

ça devrait renvoyer un certificat:

```
-----BEGIN CERTIFICATE-----
MIID........
-----END CERTIFICATE-----
```

Copiez ce certificat dans le champ "CA Certificate"

On va ensuite se créer un deuxième utilisateur admin pour gitlab:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
```

Je l'ai enregistré dans `gitlab-admin-service-account.yaml`.

Appliquez les modifications.

```sh
microk8s kubectl apply -f gitlab-admin-service-account.yaml
```

```
serviceaccount/gitlab created
clusterrolebinding.rbac.authorization.k8s.io/gitlab-admin created
```

On récupère le token

```sh
microk8s kubectl -n kube-system get secret $(microk8s kubectl -n kube-system get sa/gitlab -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
```

Et ce token vous le mettez dans le "Service Token".

Le reste vous le laissez tel quel.

Puis "Add Kubernetes cluster" !

ça y est Gitlab et votre Cluster parlent ensemble!

Installons l'éxécutor Gitlab dessus.

Ceci ce fait dans l'onglet "Applications"

![](../assets/images/kubernetes-gitlab/cluster1.png)

Descendez un peu vous verrez l'application "Gitlab Runner". Cliquez sur "Install"

![](../assets/images/kubernetes-gitlab/cluster2.png)

Au bout d'un petit moment ça aura fini de mouliner.

Rendez vous dans votre dashboard kubernetes.

![](../assets/images/kubernetes-gitlab/dashboard1.png)

Dans les espaces de noms vous devriez voir "gitlab-manages-apps", si c'est le cas, Félicitations c'est bon.

![](../assets/images/kubernetes-gitlab/dashboard2.png)

Et le runner gilab en lui même.

![](../assets/images/kubernetes-gitlab/dashboard3.png)

Si vous retournez dans la définition des runners, vous allez le retrouver :)

![](../assets/images/kubernetes-gitlab/gitlab5.png)

Si vous lancez un pipeline, vous devriez voir quelque chose comme cela.

![](../assets/images/kubernetes-gitlab/pipeline2.png)

Tous les jobs de la "step2" sont en parallèles et vous n'avez qu'à mettre les pieds sous la table ! :D

Et voilà !!

## Conclusion

Beaucoup de configurations dans cet article!

J'espère que vous avez pris autant de plaisir à le lire que moi à l'écrire.

 Merci de m'avoir lu et à la prochaine <3

 <Vssue :title="Commentaires" />
